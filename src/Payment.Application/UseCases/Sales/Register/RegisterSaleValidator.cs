﻿using FluentValidation;
using Payment.Communication.Requests;

namespace Payment.Application.UseCases.Sales.Register;

public class RegisterSaleValidator : AbstractValidator<RegisterSaleRequestJson>
{
    public RegisterSaleValidator()
    {
        RuleFor(s => s.SellerName)
            .NotEmpty().WithMessage("Nome do vendedor é obrigatório.");

        RuleFor(s => s.SellerCpf)
            .NotEmpty().WithMessage("CPF do vendedor é obrigatório.")
            .Length(11).WithMessage("CPF do vendedor deve conter 11 caracteres.")
            .Matches(@"^[0-9]*$").WithMessage("CPF do vendedor deve conter apenas números.");

        RuleFor(s => s.SellerEmail)
            .NotEmpty().WithMessage("Email do vendedor é obrigatório.")
            .EmailAddress().WithMessage("Email do vendedor inválido.");

        RuleFor(s => s.SellerPhone)
            .NotEmpty().WithMessage("Telefone do vendedor é obrigatório.")
            .Length(11).WithMessage("Telefone do vendedor deve conter 11 caracteres.")
            .Matches(@"^[0-9]*$").WithMessage("Telefone do vendedor deve conter apenas números.");

        RuleFor(s => s.SoldItems)
            .Must(items => items.Count > 0).WithMessage("É necessário conter pelo menos um item vendido.");

        RuleForEach(s => s.SoldItems)
            .ChildRules(item =>
            {
                item.RuleFor(s => s.Name)
                    .NotEmpty().WithMessage("Nome do item vendido é obrigatório.");
            });
    }
}
