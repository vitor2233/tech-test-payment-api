using AutoMapper;
using Payment.Communication.Requests;
using Payment.Communication.Responses;
using Payment.Domain.Entities;
using Payment.Domain.Repositories;
using Payment.Domain.Repositories.Items;
using Payment.Domain.Repositories.Sales;
using Payment.Domain.Repositories.Sellers;
using Payment.Exception.ExceptionBase;

namespace Payment.Application.UseCases.Sales.Register;

public class RegisterSaleUseCase : IRegisterSaleUseCase
{
    private readonly IMapper _mapper;
    private readonly ISalesRepository _salesRepository;
    private readonly ISellersRepository _sellersRepository;
    private readonly IItemsRepository _itemsRepository;
    private readonly IUnitOfWork _unitOfWork;

    public RegisterSaleUseCase(IMapper mapper, ISalesRepository salesRepository,
        ISellersRepository sellersRepository, IItemsRepository itemsRepository,
        IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _salesRepository = salesRepository;
        _sellersRepository = sellersRepository;
        _itemsRepository = itemsRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<RegisterSaleResponseJson> Execute(RegisterSaleRequestJson request)
    {
        Validate(request);

        var seller = _mapper.Map<Seller>(request);
        await _sellersRepository.Add(seller);

        var sale = new Sale
        {
            Date = DateTime.UtcNow,
            SellerId = seller.Id,
            SaleStatus = Domain.Enums.SaleStatus.PendingPayment,
        };
        await _salesRepository.Add(sale);

        var soldItems = new List<Item>();
        foreach (var itemSold in request.SoldItems)
        {
            var soldItem = _mapper.Map<Item>(itemSold);
            soldItem.SaleId = sale.Id;
            soldItems.Add(soldItem);
        }

        await _itemsRepository.AddRange(soldItems);

        await _unitOfWork.Commit();

        return new RegisterSaleResponseJson
        {
            OrderIdentifier = sale.Id
        };
    }

    private void Validate(RegisterSaleRequestJson request)
    {
        var result = new RegisterSaleValidator().Validate(request);

        if (!result.IsValid)
        {
            var errorMessages = result.Errors.Select(e => e.ErrorMessage).ToList();

            throw new ErrorOnValidationException(errorMessages);
        }
    }
}