using Payment.Communication.Requests;
using Payment.Communication.Responses;

namespace Payment.Application.UseCases.Sales.Register;

public interface IRegisterSaleUseCase
{
    Task<RegisterSaleResponseJson> Execute(RegisterSaleRequestJson request);
}