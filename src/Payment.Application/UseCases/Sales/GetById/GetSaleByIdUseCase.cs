using AutoMapper;
using Payment.Communication.Responses;
using Payment.Domain.Repositories.Sales;
using Payment.Exception.ExceptionBase;

namespace Payment.Application.UseCases.Sales.GetById;

public class GetSaleByIdUseCase : IGetSaleByIdUseCase
{
    private readonly ISalesRepository _salesRepository;
    private readonly IMapper _mapper;

    public GetSaleByIdUseCase(ISalesRepository salesRepository, IMapper mapper)
    {
        _salesRepository = salesRepository;
        _mapper = mapper;
    }
    public async Task<SaleDetailResponseJson> Execute(Guid id)
    {
        var sale = await _salesRepository.GetDetail(id);
        if (sale is null)
        {
            throw new NotFoundException("Venda não encontrada");
        }
        return _mapper.Map<SaleDetailResponseJson>(sale);
    }
}