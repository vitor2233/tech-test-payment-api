using Payment.Communication.Responses;

namespace Payment.Application.UseCases.Sales.GetById;

public interface IGetSaleByIdUseCase
{
    Task<SaleDetailResponseJson> Execute(Guid id);
}