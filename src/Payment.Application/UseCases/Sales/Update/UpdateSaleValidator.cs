﻿using FluentValidation;
using Payment.Communication.Requests;

namespace Payment.Application.UseCases.Sales.Update;

public class UpdateSaleValidator : AbstractValidator<UpdateSaleRequestJson>
{
    public UpdateSaleValidator()
    {
        RuleFor(sale => sale.SaleStatus).IsInEnum().WithMessage("Status da venda inválido");
    }
}
