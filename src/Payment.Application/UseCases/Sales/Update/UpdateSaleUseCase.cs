using AutoMapper;
using FluentValidation.Results;
using Payment.Communication.Requests;
using Payment.Communication.Responses;
using Payment.Domain.Entities;
using Payment.Domain.Extensions;
using Payment.Domain.Repositories;
using Payment.Domain.Repositories.Sales;
using Payment.Exception.ExceptionBase;

namespace Payment.Application.UseCases.Sales.Update;

public class UpdateSaleUseCase : IUpdateSaleUseCase
{
    private readonly ISalesRepository _salesRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UpdateSaleUseCase(ISalesRepository salesRepository, IMapper mapper, IUnitOfWork unitOfWork)
    {
        _salesRepository = salesRepository;
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }

    public async Task<UpdateSaleResponseJson> Execute(Guid id, UpdateSaleRequestJson request)
    {
        var sale = await _salesRepository.GetById(id);

        if (sale is null)
        {
            throw new NotFoundException("Venda não encontrada");
        }


        Validate(sale.SaleStatus, request);

        var saleStatus = _mapper.Map<Domain.Enums.SaleStatus>(request.SaleStatus);
        sale.SaleStatus = saleStatus;
        _salesRepository.Update(sale);
        await _unitOfWork.Commit();

        return new UpdateSaleResponseJson
        {
            SaleStatus = sale.SaleStatus.SaleStatusToString()
        };
    }

    private void Validate(Domain.Enums.SaleStatus currentSaleStatus, UpdateSaleRequestJson request)
    {
        var result = new UpdateSaleValidator().Validate(request);

        if (result.Errors.Count < 1)
        {
            var isValidTransition = IsValidStatusTransition(currentSaleStatus, request.SaleStatus);
            if (!isValidTransition)
            {
                var givenSaleStatus = _mapper.Map<Domain.Enums.SaleStatus>(request.SaleStatus);
                var errorMessage = $"Não é possível alterar o status '{currentSaleStatus.SaleStatusToString()}' para '{givenSaleStatus.SaleStatusToString()}'";
                result.Errors.Add(new ValidationFailure(string.Empty, errorMessage));
            }
        }

        if (!result.IsValid)
        {
            var errorMessages = result.Errors.Select(e => e.ErrorMessage).ToList();

            throw new ErrorOnValidationException(errorMessages);
        }
    }

    private bool IsValidStatusTransition(Domain.Enums.SaleStatus currentStatus, Communication.Enums.SaleStatus newStatus)
    {
        return currentStatus switch
        {
            Domain.Enums.SaleStatus.PendingPayment => newStatus == Communication.Enums.SaleStatus.PaymentApproved || newStatus == Communication.Enums.SaleStatus.Cancelled,
            Domain.Enums.SaleStatus.PaymentApproved => newStatus == Communication.Enums.SaleStatus.ShippedToCarrier || newStatus == Communication.Enums.SaleStatus.Cancelled,
            Domain.Enums.SaleStatus.ShippedToCarrier => newStatus == Communication.Enums.SaleStatus.Delivered,
            _ => false,
        };
    }
}