using Payment.Communication.Requests;
using Payment.Communication.Responses;

namespace Payment.Application.UseCases.Sales.Update;

public interface IUpdateSaleUseCase
{
    Task<UpdateSaleResponseJson> Execute(Guid id, UpdateSaleRequestJson request);
}