
using AutoMapper;
using Payment.Communication.Common;
using Payment.Communication.Requests;
using Payment.Communication.Responses;
using Payment.Domain.Entities;
using Payment.Domain.Extensions;

namespace Payment.Application.AutoMapper;
public class AutoMapping : Profile
{
    public AutoMapping()
    {
        RequestToEntity();
        EntityToResponse();
    }

    private void RequestToEntity()
    {
        CreateMap<RegisterSaleRequestJson, Seller>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.SellerName))
            .ForMember(dest => dest.Cpf, opt => opt.MapFrom(src => src.SellerCpf))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.SellerEmail))
            .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.SellerPhone));

        CreateMap<ItemJson, Item>();
        CreateMap<Communication.Enums.SaleStatus, Domain.Enums.SaleStatus>();
        CreateMap<Communication.Enums.SaleStatus, Domain.Enums.SaleStatus>()
            .ConvertUsing(src => (Domain.Enums.SaleStatus)src);
    }

    private void EntityToResponse()
    {
        CreateMap<Sale, SaleDetailResponseJson>()
            .ForMember(dest => dest.Seller, opt => opt.MapFrom(src => src.Seller))
            .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src.Items))
            .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
            .ForMember(dest => dest.SaleStatus, opt => opt.MapFrom(src => src.SaleStatus.SaleStatusToString()));

        CreateMap<Seller, SellerResponseJson>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Cpf, opt => opt.MapFrom(src => src.Cpf))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

        CreateMap<Item, ItemJson>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));
    }
}