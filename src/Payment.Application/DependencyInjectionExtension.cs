using Microsoft.Extensions.DependencyInjection;
using Payment.Application.AutoMapper;
using Payment.Application.UseCases.Sales.GetById;
using Payment.Application.UseCases.Sales.Register;
using Payment.Application.UseCases.Sales.Update;

namespace Payment.Application;
public static class DependencyInjectionExtension
{
    public static void AddApplication(this IServiceCollection services)
    {
        AddAutoMapper(services);
        AddUseCases(services);
    }

    private static void AddAutoMapper(IServiceCollection services)
    {
        services.AddAutoMapper(typeof(AutoMapping));
    }

    private static void AddUseCases(IServiceCollection services)
    {
        services.AddScoped<IRegisterSaleUseCase, RegisterSaleUseCase>();
        services.AddScoped<IGetSaleByIdUseCase, GetSaleByIdUseCase>();
        services.AddScoped<IUpdateSaleUseCase, UpdateSaleUseCase>();
    }
}