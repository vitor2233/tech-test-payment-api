namespace Payment.Communication.Responses;

public class RegisterSaleResponseJson
{
    public Guid OrderIdentifier { get; set; } = Guid.Empty;
}