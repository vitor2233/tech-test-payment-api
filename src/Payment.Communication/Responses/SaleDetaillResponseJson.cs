using Payment.Communication.Common;

namespace Payment.Communication.Responses;

public class SaleDetailResponseJson
{
    public SellerResponseJson Seller { get; set; } = default!;
    public List<ItemJson> Items { get; set; } = [];
    public DateTime Date { get; set; }
    public string SaleStatus { get; set; } = string.Empty;
}