namespace Payment.Communication.Enums;

public enum SaleStatus
{
    PendingPayment = 0,
    PaymentApproved = 1,
    ShippedToCarrier = 2,
    Delivered = 3,
    Cancelled = 4
}
