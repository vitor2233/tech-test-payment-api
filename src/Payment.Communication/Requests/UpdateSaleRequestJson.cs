using Payment.Communication.Enums;

namespace Payment.Communication.Requests;

public class UpdateSaleRequestJson
{
    public SaleStatus SaleStatus { get; set; }
}