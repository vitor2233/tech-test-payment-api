using Payment.Communication.Common;

namespace Payment.Communication.Requests;

public class RegisterSaleRequestJson
{
    public string SellerName { get; set; } = string.Empty;
    public string SellerCpf { get; set; } = string.Empty;
    public string SellerEmail { get; set; } = string.Empty;
    public string SellerPhone { get; set; } = string.Empty;
    public List<ItemJson> SoldItems { get; set; } = [];
}
