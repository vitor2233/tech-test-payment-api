using Payment.Communication.Responses;
using Microsoft.AspNetCore.Mvc;
using Payment.Application.UseCases.Sales.GetById;
using Payment.Application.UseCases.Sales.Register;
using Payment.Application.UseCases.Sales.Update;
using Payment.Communication.Requests;

namespace Payment.API.Controllers;

[ApiController]
[Route("[controller]")]
public class SalesController : ControllerBase
{
    [HttpGet]
    [Route("{id}")]
    [ProducesResponseType(typeof(SaleDetailResponseJson), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorResponseJson), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetById(
        [FromServices] IGetSaleByIdUseCase useCase,
        [FromRoute] Guid id)
    {
        var response = await useCase.Execute(id);

        return Ok(response);
    }

    [HttpPost]
    [ProducesResponseType(typeof(RegisterSaleResponseJson), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErrorResponseJson), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Register(
        [FromServices] IRegisterSaleUseCase useCase,
        [FromBody] RegisterSaleRequestJson request)
    {
        var response = await useCase.Execute(request);
        return Created(string.Empty, response);
    }

    [HttpPatch]
    [Route("status/{id}")]
    [ProducesResponseType(typeof(UpdateSaleResponseJson), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorResponseJson), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorResponseJson), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update(
        [FromServices] IUpdateSaleUseCase useCase,
        [FromRoute] Guid id,
        [FromBody] UpdateSaleRequestJson request)
    {
        var response = await useCase.Execute(id, request);

        return Ok(response);
    }
}