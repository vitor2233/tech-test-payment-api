﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Payment.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "T_SELLER",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Cpf = table.Column<string>(type: "text", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: false),
                    Phone = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_SELLER", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_SALE",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    OrderIdentifier = table.Column<string>(type: "text", nullable: false),
                    SaleStatus = table.Column<int>(type: "integer", nullable: false),
                    SellerId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_SALE", x => x.Id);
                    table.ForeignKey(
                        name: "FK_T_SALE_T_SELLER_SellerId",
                        column: x => x.SellerId,
                        principalTable: "T_SELLER",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "T_ITEM",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    SaleId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_ITEM", x => x.Id);
                    table.ForeignKey(
                        name: "FK_T_ITEM_T_SALE_SaleId",
                        column: x => x.SaleId,
                        principalTable: "T_SALE",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_T_ITEM_SaleId",
                table: "T_ITEM",
                column: "SaleId");

            migrationBuilder.CreateIndex(
                name: "IX_T_SALE_SellerId",
                table: "T_SALE",
                column: "SellerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "T_ITEM");

            migrationBuilder.DropTable(
                name: "T_SALE");

            migrationBuilder.DropTable(
                name: "T_SELLER");
        }
    }
}
