using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Payment.Domain.Repositories;
using Payment.Domain.Repositories.Items;
using Payment.Domain.Repositories.Sales;
using Payment.Domain.Repositories.Sellers;
using Payment.Infrastructure.DataAccess;
using Payment.Infrastructure.DataAccess.Repositories;

namespace Payment.Infrastructure;

public static class DependencyInjectionExtension
{
    public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        AddDbContext(services, configuration);
        AddRepositories(services);
    }

    private static void AddDbContext(IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("Connection");
        services.AddDbContext<PaymentDbContext>(config => config.UseNpgsql(connectionString));
    }

    private static void AddRepositories(IServiceCollection services)
    {
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<ISalesRepository, SalesRepository>();
        services.AddScoped<ISellersRepository, SellersRepository>();
        services.AddScoped<IItemsRepository, ItemsRepository>();
    }
}