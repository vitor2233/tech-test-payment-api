using Payment.Domain.Entities;
using Payment.Domain.Repositories.Items;

namespace Payment.Infrastructure.DataAccess.Repositories;

internal class ItemsRepository : IItemsRepository
{
    private readonly PaymentDbContext _context;
    public ItemsRepository(PaymentDbContext context)
    {
        _context = context;
    }

    public async Task AddRange(List<Item> items)
    {
        await _context.Items.AddRangeAsync(items);
    }
}