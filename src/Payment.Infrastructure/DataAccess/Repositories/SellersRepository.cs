using Payment.Domain.Entities;
using Payment.Domain.Repositories.Sellers;

namespace Payment.Infrastructure.DataAccess.Repositories;

internal class SellersRepository : ISellersRepository
{
    private readonly PaymentDbContext _context;
    public SellersRepository(PaymentDbContext context)
    {
        _context = context;
    }

    public async Task Add(Seller seller)
    {
        await _context.Sellers.AddAsync(seller);
    }
}