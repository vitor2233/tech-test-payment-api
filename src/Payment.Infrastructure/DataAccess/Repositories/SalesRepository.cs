using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;
using Payment.Domain.Enums;
using Payment.Domain.Repositories.Sales;

namespace Payment.Infrastructure.DataAccess.Repositories;

internal class SalesRepository : ISalesRepository
{
    private readonly PaymentDbContext _context;
    public SalesRepository(PaymentDbContext context)
    {
        _context = context;
    }

    public async Task Add(Sale sale)
    {
        await _context.Sales.AddAsync(sale);
    }

    public void Update(Sale sale)
    {
        _context.Sales.Update(sale);
    }

    public async Task<Sale?> GetById(Guid id)
    {
        return await _context.Sales.FirstOrDefaultAsync(s => s.Id == id);
    }

    public async Task<Sale?> GetDetail(Guid id)
    {
        return await _context.Sales.AsNoTracking().Where(s => s.Id == id)
            .Include(s => s.Items).Include(s => s.Seller).FirstOrDefaultAsync();
    }
}