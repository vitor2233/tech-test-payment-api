using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Infrastructure.DataAccess;

internal class PaymentDbContext : DbContext
{
    public PaymentDbContext(DbContextOptions options) : base(options) { }
    public DbSet<Seller> Sellers { get; set; }
    public DbSet<Sale> Sales { get; set; }
    public DbSet<Item> Items { get; set; }
}