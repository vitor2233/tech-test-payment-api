
using Payment.Domain.Repositories;

namespace Payment.Infrastructure.DataAccess;

internal class UnitOfWork : IUnitOfWork
{
    private readonly PaymentDbContext _dbContext;
    public UnitOfWork(PaymentDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Commit() => await _dbContext.SaveChangesAsync();
}