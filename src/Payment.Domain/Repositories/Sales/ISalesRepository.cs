
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories.Sales;

public interface ISalesRepository
{
    Task Add(Sale sale);
    void Update(Sale sale);
    Task<Sale?> GetById(Guid id);
    Task<Sale?> GetDetail(Guid id);
}