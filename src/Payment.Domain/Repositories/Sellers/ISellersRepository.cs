
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories.Sellers;

public interface ISellersRepository
{
    Task Add(Seller seller);
}