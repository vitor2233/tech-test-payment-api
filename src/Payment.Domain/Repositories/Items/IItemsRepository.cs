
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories.Items;

public interface IItemsRepository
{
    Task AddRange(List<Item> items);
}