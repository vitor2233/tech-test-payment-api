
namespace Payment.Domain.Repositories;
public interface IUnitOfWork
{
    Task Commit();
}