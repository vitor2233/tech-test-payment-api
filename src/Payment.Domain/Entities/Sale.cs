using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Payment.Domain.Enums;

namespace Payment.Domain.Entities;

[Table("T_SALE")]
public class Sale
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public Guid Id { get; set; }
    public DateTime Date { get; set; }
    public SaleStatus SaleStatus { get; set; }
    public Guid SellerId { get; set; }
    public Seller Seller { get; set; } = default!;
    public ICollection<Item> Items { get; set; } = [];
}