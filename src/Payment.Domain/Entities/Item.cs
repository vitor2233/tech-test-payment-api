using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Payment.Domain.Entities;

[Table("T_ITEM")]
public class Item
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Guid SaleId { get; set; }
    public Sale Sale { get; set; } = default!;
}