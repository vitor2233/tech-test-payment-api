using Payment.Domain.Enums;

namespace Payment.Domain.Extensions;
public static class SaleStatusExtensions
{
    public static string SaleStatusToString(this SaleStatus saleStatus)
    {
        return saleStatus switch
        {
            SaleStatus.PendingPayment => "Aguardando pagamento",
            SaleStatus.PaymentApproved => "Pagamento Aprovado",
            SaleStatus.ShippedToCarrier => "Enviado para Transportadora",
            SaleStatus.Delivered => "Entregue",
            SaleStatus.Cancelled => "Cancelada",
            _ => string.Empty
        };
    }
}