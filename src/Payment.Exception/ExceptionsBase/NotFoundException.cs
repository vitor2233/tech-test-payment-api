﻿using System.Net;

namespace Payment.Exception.ExceptionBase;

public class NotFoundException : PaymentException
{
    public NotFoundException(string message) : base(message) { }

    public override int StatusCode => (int)HttpStatusCode.NotFound;

    public override List<string> GetErrors()
    {
        return new List<string>() { Message };
    }
}
