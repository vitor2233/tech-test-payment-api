﻿namespace Payment.Exception.ExceptionBase;

public abstract class PaymentException : SystemException
{
    protected PaymentException(string message) : base(message) { }

    public abstract int StatusCode { get; }
    public abstract List<string> GetErrors();
}
