
using Common.Utilities.Mapper;
using Common.Utilities.Repositories;
using Common.Utilities.Requests;
using FluentAssertions;
using Payment.Application.UseCases.Sales.Register;

namespace UseCases.Test.Sales.Register;

public class RegisterSaleUseCaseTest
{
    [Fact]
    public async Task Success()
    {
        var request = RegisterSaleRequestJsonBuilder.Build();
        var useCase = CreateUseCase();

        var result = await useCase.Execute(request);

        result.Should().NotBeNull();
        result.OrderIdentifier.Should().NotBeEmpty();
    }

    private RegisterSaleUseCase CreateUseCase()
    {
        var mapper = MapperBuilder.Build();
        var unitOfWork = UnitOfWorkBuilder.Build();
        var itemsRepository = ItemsRepositoryBuilder.Build();
        var sellersRepository = SellersRepositoryBuilder.Build();
        var salesRepository = new SalesRepositoryBuilder();
        salesRepository.AddSale();
        return new RegisterSaleUseCase(mapper, salesRepository.Build(), sellersRepository, itemsRepository, unitOfWork);
    }
}