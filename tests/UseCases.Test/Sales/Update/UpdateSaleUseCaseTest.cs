
using Common.Utilities.Entities;
using Common.Utilities.Mapper;
using Common.Utilities.Repositories;
using Common.Utilities.Requests;
using FluentAssertions;
using Payment.Application.UseCases.Sales.Update;
using Payment.Domain.Entities;
using Payment.Exception.ExceptionBase;

namespace UseCases.Test.Sales.Update;

public class UpdateSaleUseCaseTest
{
    [Fact]
    public async Task Success()
    {
        var sale = SaleBuilder.Build();
        var request = UpdateSaleRequestJsonBuilder.Build();
        request.SaleStatus = Payment.Communication.Enums.SaleStatus.PaymentApproved;
        var useCase = CreateUseCase(sale);

        var result = await useCase.Execute(sale.Id, request);

        result.Should().NotBeNull();
        result.SaleStatus.Should().NotBeNullOrWhiteSpace();
    }

    [Fact]
    public async Task Should_Fail_When_Id_Is_Invalid()
    {
        var sale = SaleBuilder.Build();
        var request = UpdateSaleRequestJsonBuilder.Build();
        var useCase = CreateUseCase(sale);

        var act = async () => await useCase.Execute(Guid.NewGuid(), request);
        var result = await act.Should().ThrowAsync<NotFoundException>();
        result.Where(e => e.GetErrors().Count == 1 && e.GetErrors().Contains("Venda não encontrada"));
    }

    [Theory]
    [InlineData(Payment.Domain.Enums.SaleStatus.PendingPayment, Payment.Communication.Enums.SaleStatus.Delivered)]
    [InlineData(Payment.Domain.Enums.SaleStatus.PendingPayment, Payment.Communication.Enums.SaleStatus.PendingPayment)]
    [InlineData(Payment.Domain.Enums.SaleStatus.PendingPayment, Payment.Communication.Enums.SaleStatus.ShippedToCarrier)]
    [InlineData(Payment.Domain.Enums.SaleStatus.PaymentApproved, Payment.Communication.Enums.SaleStatus.Delivered)]
    [InlineData(Payment.Domain.Enums.SaleStatus.PaymentApproved, Payment.Communication.Enums.SaleStatus.PaymentApproved)]
    [InlineData(Payment.Domain.Enums.SaleStatus.PaymentApproved, Payment.Communication.Enums.SaleStatus.PendingPayment)]
    [InlineData(Payment.Domain.Enums.SaleStatus.ShippedToCarrier, Payment.Communication.Enums.SaleStatus.ShippedToCarrier)]
    [InlineData(Payment.Domain.Enums.SaleStatus.ShippedToCarrier, Payment.Communication.Enums.SaleStatus.Cancelled)]
    [InlineData(Payment.Domain.Enums.SaleStatus.ShippedToCarrier, Payment.Communication.Enums.SaleStatus.PaymentApproved)]
    [InlineData(Payment.Domain.Enums.SaleStatus.ShippedToCarrier, Payment.Communication.Enums.SaleStatus.PendingPayment)]
    public async Task Should_Fail_When_Transition_Is_Invalid(Payment.Domain.Enums.SaleStatus currentDomainStatus, Payment.Communication.Enums.SaleStatus invalidCommunicationStatus)
    {
        var sale = SaleBuilder.Build();
        sale.SaleStatus = currentDomainStatus;
        var request = UpdateSaleRequestJsonBuilder.Build();
        request.SaleStatus = invalidCommunicationStatus;

        var useCase = CreateUseCase(sale);

        var act = async () => await useCase.Execute(sale.Id, request);

        var result = await act.Should().ThrowAsync<ErrorOnValidationException>();
        result.Where(e => e.GetErrors().Count == 1);
    }

    private UpdateSaleUseCase CreateUseCase(Sale sale)
    {
        var mapper = MapperBuilder.Build();
        var unitOfWork = UnitOfWorkBuilder.Build();
        var salesRepository = new SalesRepositoryBuilder();
        salesRepository.GetSaleById(sale);
        return new UpdateSaleUseCase(salesRepository.Build(), mapper, unitOfWork);
    }
}