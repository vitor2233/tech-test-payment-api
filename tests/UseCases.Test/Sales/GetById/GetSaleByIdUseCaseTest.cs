
using Common.Utilities.Entities;
using Common.Utilities.Mapper;
using Common.Utilities.Repositories;
using Common.Utilities.Requests;
using FluentAssertions;
using Payment.Application.UseCases.Sales.GetById;
using Payment.Domain.Entities;
using Payment.Exception.ExceptionBase;

namespace UseCases.Test.Sales.GetById;

public class GetSaleByIdUseCaseTest
{
    [Fact]
    public async Task Success()
    {
        var sale = SaleBuilder.Build();
        var useCase = CreateUseCase(sale);

        var result = await useCase.Execute(sale.Id);

        result.Should().NotBeNull();
        result.SaleStatus.Should().NotBeNullOrWhiteSpace();
        result.Seller.Name.Should().NotBeNullOrWhiteSpace();
        result.Seller.Cpf.Should().NotBeNullOrWhiteSpace();
        result.Seller.Email.Should().NotBeNullOrWhiteSpace();
        result.Seller.Phone.Should().NotBeNullOrWhiteSpace();
    }

    [Fact]
    public async Task Should_Fail_When_Id_Is_Invalid()
    {
        var sale = SaleBuilder.Build();
        var request = UpdateSaleRequestJsonBuilder.Build();
        var useCase = CreateUseCase(sale);

        var act = async () => await useCase.Execute(Guid.NewGuid());
        var result = await act.Should().ThrowAsync<NotFoundException>();
        result.Where(e => e.GetErrors().Count == 1 && e.GetErrors().Contains("Venda não encontrada"));
    }

    private GetSaleByIdUseCase CreateUseCase(Sale sale)
    {
        var mapper = MapperBuilder.Build();
        var salesRepository = new SalesRepositoryBuilder();
        salesRepository.GetSaleDetail(sale);
        return new GetSaleByIdUseCase(salesRepository.Build(), mapper);
    }
}