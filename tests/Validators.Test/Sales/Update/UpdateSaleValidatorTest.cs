using Common.Utilities.Requests;
using FluentAssertions;
using Payment.Application.UseCases.Sales.Update;
using Payment.Communication.Enums;

namespace Validators.Test.Sales.Update;

public class UpdateSaleValidatorTest
{
    [Fact]
    public void Success()
    {
        var validator = new UpdateSaleValidator();
        var request = UpdateSaleRequestJsonBuilder.Build();

        var result = validator.Validate(request);

        result.IsValid.Should().BeTrue();
    }

    [Theory]
    [InlineData(-1)]
    [InlineData(100)]
    public void Error_InvalidSaleStatus(int invalidStatus)
    {
        var validator = new UpdateSaleValidator();
        var request = UpdateSaleRequestJsonBuilder.Build();
        request.SaleStatus = (SaleStatus)invalidStatus;

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().ContainSingle().And.Contain(e => e.ErrorMessage.Equals("Status da venda inválido"));
    }
}