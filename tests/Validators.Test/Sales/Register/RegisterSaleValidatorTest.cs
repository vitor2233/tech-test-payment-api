using Payment.Application.UseCases.Sales.Register;

using FluentAssertions;
using Common.Utilities.Requests;
using Payment.Communication.Common;

namespace Validators.Test.Sales.Register;

public class RegisterSaleValidatorTest
{
    [Fact]
    public void Success()
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();

        var result = validator.Validate(request);

        result.IsValid.Should().BeTrue();
    }

    [Theory]
    [InlineData("")]
    [InlineData("   ")]
    [InlineData(null)]
    public void Error_Name_Empty(string name)
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();
        request.SellerName = name;

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().ContainSingle().And.Contain(e => e.ErrorMessage.Equals("Nome do vendedor é obrigatório."));
    }

    [Theory]
    [InlineData("")]
    [InlineData("1234567890")] // Menos de 11 caracteres
    [InlineData("123456789012")] // Mais de 11 caracteres
    [InlineData("1234567890a")] // Contém caracteres não numéricos
    public void Error_Cpf_Invalid(string cpf)
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();
        request.SellerCpf = cpf;

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().Contain(e => e.ErrorMessage.Contains("CPF do vendedor"));
    }

    [Theory]
    [InlineData("")]
    [InlineData("1234567890")] // Menos de 11 caracteres
    [InlineData("123456789012")] // Mais de 11 caracteres
    [InlineData("1234567890a")] // Contém caracteres não numéricos
    public void Error_Phone_Invalid(string phone)
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();
        request.SellerPhone = phone;

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().Contain(e => e.ErrorMessage.Contains("Telefone do vendedor"));
    }

    [Theory]
    [InlineData("")]
    [InlineData("invalid-email")]
    [InlineData("@missingusername.com")]
    public void Error_Email_Invalid(string email)
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();
        request.SellerEmail = email;

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().Contain(e => e.ErrorMessage.Contains("Email do vendedor"));
    }

    [Fact]
    public void Error_SoldItems_Empty()
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();
        request.SoldItems = new List<ItemJson>();

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().Contain(e => e.ErrorMessage.Equals("É necessário conter pelo menos um item vendido."));
    }

    [Theory]
    [InlineData("")]
    [InlineData(null)]
    public void Error_SoldItemName_Empty(string itemName)
    {
        var validator = new RegisterSaleValidator();
        var request = RegisterSaleRequestJsonBuilder.Build();
        request.SoldItems = new List<ItemJson>
        {
            new ItemJson { Name = itemName }
        };

        var result = validator.Validate(request);

        result.IsValid.Should().BeFalse();
        result.Errors.Should().Contain(e => e.ErrorMessage.Equals("Nome do item vendido é obrigatório."));
    }
}