
using AutoMapper;
using Payment.Application.AutoMapper;

namespace Common.Utilities.Mapper;
public static class MapperBuilder
{
    public static IMapper Build()
    {
        var mapper = new MapperConfiguration(c =>
        {
            c.AddProfile(new AutoMapping());
        });

        return mapper.CreateMapper();
    }
}