﻿using Bogus;
using Payment.Domain.Entities;

namespace Common.Utilities.Entities;
public static class SellerBuilder
{
    public static Seller Build()
    {
        var sale = new Faker<Seller>()
            .RuleFor(s => s.Id, _ => Guid.NewGuid())
            .RuleFor(r => r.Name, faker => faker.Person.FirstName)
            .RuleFor(r => r.Cpf, faker => faker.Random.ReplaceNumbers("###########"))
            .RuleFor(r => r.Phone, faker => faker.Random.ReplaceNumbers("###########"))
            .RuleFor(r => r.Email, faker => faker.Internet.Email());
        return sale;
    }
}
