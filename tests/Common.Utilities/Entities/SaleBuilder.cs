﻿using Bogus;
using Payment.Domain.Entities;
using Payment.Domain.Enums;

namespace Common.Utilities.Entities;
public static class SaleBuilder
{
    public static Sale Build()
    {
        var sale = new Faker<Sale>()
            .RuleFor(s => s.Id, _ => Guid.NewGuid())
            .RuleFor(s => s.Date, _ => DateTime.UtcNow)
            .RuleFor(s => s.SaleStatus, _ => SaleStatus.PendingPayment);
        return sale;
    }
}
