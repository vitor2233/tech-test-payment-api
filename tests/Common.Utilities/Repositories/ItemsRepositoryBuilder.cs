using Moq;
using Payment.Domain.Repositories.Items;

namespace Common.Utilities.Repositories;
public class ItemsRepositoryBuilder
{
    public static IItemsRepository Build()
    {
        var mock = new Mock<IItemsRepository>();
        return mock.Object;
    }
}