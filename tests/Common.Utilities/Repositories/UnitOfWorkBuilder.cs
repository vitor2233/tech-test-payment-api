using Moq;
using Payment.Domain.Repositories;

namespace Common.Utilities.Repositories;
public static class UnitOfWorkBuilder
{
    public static IUnitOfWork Build()
    {
        var mock = new Mock<IUnitOfWork>();
        return mock.Object;
    }
}