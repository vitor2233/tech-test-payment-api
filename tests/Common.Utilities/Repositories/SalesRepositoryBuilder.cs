using Common.Utilities.Entities;
using Moq;
using Payment.Domain.Entities;
using Payment.Domain.Repositories.Sales;

namespace Common.Utilities.Repositories;
public class SalesRepositoryBuilder
{
    private readonly Mock<ISalesRepository> _repository;

    public SalesRepositoryBuilder()
    {
        _repository = new Mock<ISalesRepository>();
    }

    public void GetSaleDetail(Sale sale)
    {
        sale.Seller = SellerBuilder.Build();
        sale.SellerId = sale.Seller.Id;
        _repository.Setup(r => r.GetDetail(sale.Id)).ReturnsAsync(sale);
    }

    public void GetSaleById(Sale sale)
    {
        _repository.Setup(r => r.GetById(sale.Id)).ReturnsAsync(sale);
    }

    public void AddSale()
    {
        _repository.Setup(r => r.Add(It.IsAny<Sale>())).Callback<Sale>(sale =>
        {
            sale.Id = Guid.NewGuid();
        });
    }

    public ISalesRepository Build() => _repository.Object;
}