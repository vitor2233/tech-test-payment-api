using Moq;
using Payment.Domain.Repositories.Sellers;

namespace Common.Utilities.Repositories;
public class SellersRepositoryBuilder
{
    public static ISellersRepository Build()
    {
        var mock = new Mock<ISellersRepository>();
        return mock.Object;
    }
}