﻿using Bogus;
using Payment.Communication.Enums;
using Payment.Communication.Requests;

namespace Common.Utilities.Requests;

public static class UpdateSaleRequestJsonBuilder
{
    public static UpdateSaleRequestJson Build()
    {
        return new Faker<UpdateSaleRequestJson>()
            .RuleFor(r => r.SaleStatus, faker => faker.PickRandom<SaleStatus>());
    }
}
