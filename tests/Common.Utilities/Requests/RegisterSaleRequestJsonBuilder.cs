﻿using Bogus;
using Payment.Communication.Common;
using Payment.Communication.Requests;

namespace Common.Utilities.Requests;
public static class RegisterSaleRequestJsonBuilder
{
    public static RegisterSaleRequestJson Build()
    {
        return new Faker<RegisterSaleRequestJson>()
            .RuleFor(r => r.SellerName, faker => faker.Person.FirstName)
            .RuleFor(r => r.SellerCpf, faker => faker.Random.ReplaceNumbers("###########"))
            .RuleFor(r => r.SellerPhone, faker => faker.Random.ReplaceNumbers("###########"))
            .RuleFor(r => r.SellerEmail, faker => faker.Internet.Email())
            .RuleFor(r => r.SoldItems, f => GenerateItems(f, 3));
    }

    private static List<ItemJson> GenerateItems(Faker faker, int count)
    {
        var items = new List<ItemJson>();

        for (int i = 0; i < count; i++)
        {
            var item = new ItemJson
            {
                Name = faker.Commerce.ProductName()
            };
            items.Add(item);
        }

        return items;
    }
}
